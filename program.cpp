#include <iostream>
#include "list.h"
using namespace std;

int main()
{
	//Sample Code
	List mylist;

    cout<<"pushToHead: "<<endl;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	mylist.print();
    cout<<endl;

	//TO DO! Complete the functions, then write a program to test them.
	//_____Adapt your code so that your list can store data other than characters - how about a template?

    cout<<"\npushToTail: "<<endl;
	mylist.pushToTail('k');
	mylist.pushToTail('e');
	mylist.pushToTail('n');
	mylist.print();
    cout<<endl;

    cout<<"\npopHead: "<<endl;
	mylist.popHead();
	mylist.print();
    cout<<endl;

    cout<<"\npopTail: "<<endl;
	mylist.popTail();
	mylist.print();
    cout<<endl;

    cout<<"\nfind the alphabet _n_ is in list?"<<endl;
    mylist.search('n');
    mylist.print();
    cout<<endl;

    cout<<"\nfind the alphabet _k_ is in list?"<<endl;
    mylist.search('k');
    mylist.print();
    cout<<endl;

}
