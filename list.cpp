#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head, 0);
	if(tail==0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	//TO DO!
	Node *pt = new Node(el, 0, 0);

	if(head == NULL)
	{
		head = pt;
		tail = pt;
	}
	else
	{
		tail -> next = pt;
		pt -> prev = tail;
		tail = tail -> next;
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	// TO DO!
    Node *pt;
	int el;

	pt = tail;
	pt = pt -> prev;
	el = tail -> data;
	delete tail;
	pt -> next = NULL;
	tail = pt;
	return el;

	//return NULL;
}
bool List::search(char el)
{
	// TO DO! (Function to return True or False depending if a character is in the list.
    Node *pt = head;
    while(pt != NULL)
    {
        if(pt -> data == el)
        {
        	cout<<"It is in the list\n";
            return true;
            break;
        }else
        {
            pt = pt -> next;
        }
    }
	cout<<"It is -not- in the list\n";
    return false;

	//return NULL;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
